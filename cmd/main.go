package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/test-works31/e-shop/internal/app"
	"gitlab.com/test-works31/e-shop/internal/command"
	"gitlab.com/test-works31/e-shop/internal/storage/mysql"
)

func main() {
	config := app.GetConfig()

	dbConnection := fmt.Sprintf(
		config.DB.ConnectionStr, config.DB.Username,
		config.DB.Password, config.DB.Server, config.DB.Port, config.DB.Name,
	)

	db, err := sql.Open("mysql", dbConnection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	shelfRepo := mysql.NewShelfRepository(db)
	service := app.NewService(shelfRepo)
	handler := command.NewHandler(service)

	orderList := os.Args
	handler.Rout(orderList)
}

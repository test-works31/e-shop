package view

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/test-works31/e-shop/internal/app"
)

func PresentShelvesWithOrders(dataStr string, ans map[string][]*app.Response) {
	buf := os.Stdout

	fmt.Fprintf(buf, "=+=+=+=\nСтраница сборки заказов %s\n\n", dataStr)

	for nameShelf, product := range ans {
		fmt.Fprintf(buf, "=== Стеллаж %s\n", nameShelf)
		for _, resp := range product {
			fmt.Fprintf(buf, "%s (id=%d)\n", resp.NameProduct, resp.ProductId)
			fmt.Fprintf(buf, "заказ %d, %v шт\n", resp.OrderId, resp.Quantity)
			if resp.AdditionalShelves[0] != "" {
				fmt.Fprintf(buf, "доп стеллаж: %v\n", strings.Join(resp.AdditionalShelves, ", "))
			}
			fmt.Fprintln(buf)
		}

	}

}

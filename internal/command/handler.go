package command

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/test-works31/e-shop/internal/app"
	"gitlab.com/test-works31/e-shop/internal/view"
)

type Handler struct {
	service app.Service
}

func NewHandler(service app.Service) *Handler {
	return &Handler{service}
}

func (h *Handler) Rout(args []string) {
	switch len(args) {
	case 2:
		h.HandleOrderList(args[1])
	default:
		fmt.Println("Используйте: go run main.go <список заказов через запятую, без пробелов>")
		return
	}
}

func (h *Handler) HandleOrderList(args string) {

	response, err := h.service.GetShelvesWithOrders(toList(args))
	if err != nil {
		fmt.Println(err)
		return
	}

	view.PresentShelvesWithOrders(args, response)
}

func toList(input string) []int64 {
	ids := strings.Split(input, ",")
	shelfIDs := make([]int64, 0, len(ids))

	for _, idStr := range ids {
		id, err := strconv.Atoi(idStr)
		if err != nil {
			fmt.Println("Используйте: go run main.go <список заказов через запятую, без пробелов>")
			return nil
		}
		shelfIDs = append(shelfIDs, int64(id))
	}

	return shelfIDs
}

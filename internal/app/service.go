package app

type Response struct {
	MainShelf         string
	NameProduct       string
	ProductId         int64
	OrderId           int64
	Quantity          int64
	AdditionalShelves []string
}

type ShelfRepository interface {
	GetOrders([]int64) ([]*Response, error)
}

type Service interface {
	GetShelvesWithOrders([]int64) (map[string][]*Response, error)
}

type service struct {
	repo ShelfRepository
}

func NewService(repo ShelfRepository) Service {
	return &service{
		repo: repo,
	}
}

func (s *service) GetShelvesWithOrders(ordersId []int64) (map[string][]*Response, error) {
	respList, err := s.repo.GetOrders(ordersId)
	if err != nil {
		return nil, err
	}
	mapResponse := make(map[string][]*Response)
	for _, resp := range respList {
		if _, ok := mapResponse[resp.MainShelf]; ok {
			mapResponse[resp.MainShelf] = append(mapResponse[resp.MainShelf], resp)
			continue
		}
		mapResponse[resp.MainShelf] = append(mapResponse[resp.MainShelf], resp)
	}
	return mapResponse, nil
}

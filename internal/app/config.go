package app

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	DB struct {
		Server        string
		Port          int
		Username      string
		Password      string
		Name          string
		ConnectionStr string
	}
}

func GetConfig() *Config {
	configPath := "../config/config.test.yml"

	if configPath == "" {
		log.Fatal("CONFIG_PATH not found.")
	}

	yamlFile, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatalf("yamlFile.Get %+v ", err)
	}
	c := &Config{}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		log.Fatalf("Unmarshal: %#v", err)
	}

	return c
}

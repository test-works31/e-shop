package mysql

import (
	"database/sql"
	"fmt"
	"strings"

	"gitlab.com/test-works31/e-shop/internal/app"
)

var productShelfMap map[int64]*ProductShelf
var nameProductById map[int64]string

type shelfRepo struct {
	db *sql.DB
}

type OrderProductDB struct {
	ProductId sql.NullInt64
	OrderId   sql.NullInt64
	Quantity  sql.NullInt64
}

type ProductShelfDB struct {
	IsMainShelf sql.NullBool
	ShelfName   sql.NullString
	ProductId   sql.NullInt64
}

type ProductShelf struct {
	ProductId int64
	MainShelf string
	AddShelf  string
}

func NewShelfRepository(db *sql.DB) app.ShelfRepository {
	return &shelfRepo{db}
}

func (sr *shelfRepo) GetOrders(orderList []int64) ([]*app.Response, error) {

	placeholder, args := getPlaceholderAndArgs(orderList)

	query := fmt.Sprintf(`SELECT po.product_id, po.order_id, po.quantity 
		FROM product_order AS po 
		WHERE order_id in (%s)`, placeholder)

	rows, err := sr.db.Query(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var productIdList []int64
	var orderProduct []*OrderProductDB

	nameProductById = make(map[int64]string)

	for rows.Next() {
		orderProductDB := &OrderProductDB{}

		err := rows.Scan(
			&orderProductDB.ProductId, &orderProductDB.OrderId, &orderProductDB.Quantity,
		)

		if err != nil {
			return nil, err
		}

		if _, ok := nameProductById[orderProductDB.ProductId.Int64]; !ok {
			nameProductById[orderProductDB.ProductId.Int64] = ""
			productIdList = append(productIdList, orderProductDB.ProductId.Int64)
		}

		orderProduct = append(orderProduct, orderProductDB)
	}

	err = sr.getProductName(productIdList)
	if err != nil {
		return nil, err
	}

	err = sr.getShelfNameByProductId(productIdList)
	if err != nil {
		return nil, err
	}

	var orders []*app.Response
	for _, order := range orderProduct {
		resp := &app.Response{
			ProductId: order.ProductId.Int64,
			OrderId:   order.OrderId.Int64,
			Quantity:  order.Quantity.Int64,
		}
		resp.MainShelf = productShelfMap[resp.ProductId].MainShelf
		resp.AdditionalShelves = strings.Split(productShelfMap[resp.ProductId].AddShelf, ",")
		resp.NameProduct = nameProductById[resp.ProductId]
		orders = append(orders, resp)
	}

	return orders, nil
}

func (sr *shelfRepo) getShelfNameByProductId(productId []int64) error {
	productShelfMap = make(map[int64]*ProductShelf)
	placeholder, args := getPlaceholderAndArgs(productId)
	query := fmt.Sprintf(
		`SELECT ps.is_main_shelf, GROUP_CONCAT(s.name), ps.product_id 
		FROM product_shelf AS ps LEFT JOIN shelf s ON ps.shelf_id=s.id 
		WHERE product_id in (%s) 
		GROUP BY product_id, is_main_shelf`, placeholder,
	)
	rows, err := sr.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		psDB := ProductShelfDB{}
		ps := ProductShelf{}
		err := rows.Scan(&psDB.IsMainShelf, &psDB.ShelfName, &psDB.ProductId)
		if err != nil {
			return err
		}

		if _, ok := productShelfMap[psDB.ProductId.Int64]; ok {
			if psDB.IsMainShelf.Bool {
				productShelfMap[psDB.ProductId.Int64].MainShelf = psDB.ShelfName.String
			} else {
				productShelfMap[psDB.ProductId.Int64].AddShelf = psDB.ShelfName.String
			}
		} else {
			ps.ProductId = psDB.ProductId.Int64
			if psDB.IsMainShelf.Bool {
				ps.MainShelf = psDB.ShelfName.String
			} else {
				ps.AddShelf = psDB.ShelfName.String
			}
			productShelfMap[psDB.ProductId.Int64] = &ps
		}
	}
	return nil
}

func (sr *shelfRepo) getProductName(productId []int64) error {
	var id sql.NullInt64
	var name sql.NullString

	placeholder, args := getPlaceholderAndArgs(productId)
	query := fmt.Sprintf(`SELECT id, name FROM product WHERE id IN (%s)`, placeholder)
	rows, err := sr.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&id, &name)
		if err != nil {
			return err
		}
		nameProductById[id.Int64] = name.String
	}
	return nil
}

func getPlaceholderAndArgs(listId []int64) (string, []any) {
	placeholder := make([]string, len(listId))
	args := make([]interface{}, len(listId))
	for i, orderID := range listId {
		placeholder[i] = "?"
		args[i] = orderID
	}
	return strings.Join(placeholder, ", "), args
}
